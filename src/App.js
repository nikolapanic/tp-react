import React, { useEffect, useImperativeHandle, useState } from 'react';
import './App.css';
import axios from 'axios';

const App = () => {
  const [pokemon, setpokemon] = useState("");
  const [pokemonData, setpokemonData] = useState([]);
  const [pokemonType, setpokemonType] = useState("");
  
  const getPokemon = async () => {
    const toArray = [];
    try {
      const url = `https://pokeapi.co/api/v2/pokemon/${pokemon}`;
      const res = await axios.get(url);
      toArray.push(res.data);
      setpokemonType(res.data.types[0].type.name);
      setpokemonData(toArray);
      console.log(res);
    }
    catch (e) {
      console.log(e);
    };
  }
  const handleChange = (e) => {
    setpokemon(e.target.value.toLowerCase())
  };
  const handleSubmit  = (e) => {
    e.preventDefault();
    getPokemon();

  }

  

  return (
    <div className="App">
      <h1 class="titre">Information sur un pokemon</h1>
      <p class="info">API utilisé : PokéAPI</p>
      <br />
      <br />
      <br />
      <br />
      <p class="projet">Mon projet, permet de recupérer les informations d'un pokemon en fonction de son nom</p>
      <form onSubmit={handleSubmit}>
        <label>
          {<input type="text" onChange={handleChange} placeholder="Nom du pokémon[EN]" /> }
          { <input type="submit" onChange={handleChange} value="Rechercher" /> }
        </label>
      </form>
      {pokemonData.map((data) => {
        return (
          <div className="container">
            <img class="pokeImg" src={data.sprites["front_default"]}/>
            <div className="divTable">
              <div className="divTableBody">
                <div className="divTableRow">
                  <div className="divTableCell">Type :</div>
                  <div className="divTableCell">{pokemonType}</div>
                </div>
                <div className="divTableRow">
                  <div className="divTableCell">Height :</div>
                  <div className="divTableCell">{" "}{Math.round(data.height * 3.9)}</div>
                </div>
                <div className="divTableRow">
                  <div className="divTableCell">Weigth :</div>
                  <div className="divTableCell">{" "}{Math.round(data.weight / 4.3)} lbs</div>
                </div>
                <div className="divTableRow">
                  <div className="divTableCell">Number of battle :</div>
                  <div className="divTableCell">{data.game_indices.length}</div>
                </div>
              </div>
            </div>
          </div>
        )
      })}
    </div>
  );
}

export default App;
